﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Otus_Homework_Lesson_17
{
    public class FileArgs : EventArgs
    {
        public FileInfo? FileInfo { get; init; }
        public Exception? Exception { get; }
        public bool Cancel { get; set; }

        public FileArgs(FileInfo? fileinfo, Exception? ex = null)
        {
            FileInfo = fileinfo;
            Exception = ex;
        }
    }
}
