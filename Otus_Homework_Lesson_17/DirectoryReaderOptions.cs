﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Otus_Homework_Lesson_17
{
    public class DirectoryReaderOptions
    {
        public string RootDir { get; init; }
        public SearchOption SearchOtpion { get; init; }
        public int FileMaxAmount { get; init; }
        public string FileSearchPattern { get; init; }
        public string DirSearchPattern { get; init; }

    }
}
