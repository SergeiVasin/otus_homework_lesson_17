﻿


namespace Otus_Homework_Lesson_17
{
    public class Program
    {
        static DirectoryReader _fileReader;
        public static async Task Main(string[] args)
        {

            //Дз п.1
            DoHomeworkTaskItem_1();

            /////////////////////
            ///Дз пп 2-4
            DoHomeworkTaskItem_2_4();
        }

        static void DoHomeworkTaskItem_1()
        {
            Console.WriteLine("--------------------Домашнее задание п.1------------------------");
            List<Building> buildings = new List<Building>
            {
                new Building
                {
                    Name = "house1",
                    Height = 32.54f,
                    Width = 100.3f
                },
                new Building
                {
                    Name = "house2",
                    Height = 12.35f,
                    Width = 20.45f
                },
                new Building
                {
                    Name = "house3",
                    Height = 102.98f,
                    Width = 29.7f
                },
                new Building
                {
                    Name = "house4",
                    Height = 64.65f,
                    Width = 67.1f
                },
                new Building
                {
                    Name = "house1",
                    Height = 73.83f,
                    Width = 40
                },
                new Building
                {
                    Name = "house5",
                    Height = 150,
                    Width = 60
                }
            };

            var maxHeightBuilding = buildings.GetMax(b => b.Height);

            Console.WriteLine($"Здание с максимальной высотой: {maxHeightBuilding?.Name}");

            var maxWidthBuilding = buildings.GetMax(b => b.Width);

            Console.WriteLine($"Здание с максимальной шириной: {maxWidthBuilding?.Name}");

            Console.WriteLine("-----------------------------------------------------------------");
        }

        static void DoHomeworkTaskItem_2_4()
        {
            Console.WriteLine("--------------------Домашнее задание п.2-4------------------------");

            _fileReader = new DirectoryReader(new DirectoryReaderOptions
            {
                RootDir = @"C:\",
                DirSearchPattern = "*",
                FileSearchPattern = "*.*",
                SearchOtpion = SearchOption.AllDirectories,
                FileMaxAmount = 100
            });
            _fileReader.FileFound += OnFileFound;

            _fileReader.GetFiles();



            Console.WriteLine("-----------------------------------------------------------------");
        }

        /////////////////////
        ///Дз пп 2-4
        public static void OnFileFound(object? sender, EventArgs e)
        {
            var args = (FileArgs)e;

            if (args.Exception != null)
            {
                Console.WriteLine();
                Console.WriteLine(args.Exception.Message);

                if (args.Exception is FileMaxCountException)
                {
                    if (IsContinue("Повысить количество файлов и продолжить?"))
                    {
                        int curAmount = _fileReader.FileMaxAmount;
                        int newAmount = curAmount;

                        while (true && newAmount <= curAmount)
                        {
                            Console.Write($"Введите новое значение для количества файлов. Оно должно быть больше текущего: {curAmount}. ");

                            if (Int32.TryParse(Console.ReadLine(), out newAmount))
                            {
                                _fileReader.FileMaxAmount = newAmount;
                                return;
                            }

                            Console.WriteLine($"Введите корректное натуральное число больше текущего = {curAmount}!");
                        }
                        return;
                    }
                    else
                    {
                        args.Cancel = true;
                        return;
                    }
                }

                if (!IsContinue("Продолжить поиск файлов?"))
                {
                    args.Cancel = true;
                }
                return;
            }

            var fileName = args?.FileInfo?.FullName;
            Console.WriteLine(fileName);
        }
        ///Дз пп 2-4
        static bool IsContinue(string mess)
        {
            bool res = false;

            while (true)
            {
                Console.WriteLine();
                Console.Write(mess + " Введите да или нет: ");
                var input = Console.ReadLine();
                if (input == "да")
                {
                    res = true;
                    break;
                }
                else if (input == "нет")
                {
                    break;
                }
            }

            return res;
        }
    }
    //Дз п.1
    public class Building
    {
        public string Name { get; set; }
        public float Height { get; set; }
        public float Width { get; set; }
    }
}