﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security;
using System.Text;
using System.Threading.Tasks;

namespace Otus_Homework_Lesson_17
{
    public class DirectoryReader
    {
        private readonly DirectoryReaderOptions _options;

        public event EventHandler FileFound;

        public int FileMaxAmount { get; set; }

        public DirectoryReader(DirectoryReaderOptions options)
        {
            _options = options;
            FileMaxAmount = _options.FileMaxAmount;
        }

        public List<FileInfo> GetFiles()
        {
            return GetAllPermissionedFiles(new DirectoryInfo(_options.RootDir), _options.DirSearchPattern, _options.FileSearchPattern, _options.SearchOtpion);
        }

        private List<FileInfo> GetAllPermissionedFiles(DirectoryInfo dirInfo, string dirSearchPattern, string fileSearchPattern, SearchOption searchOption)
        {
            var fileInfos = new List<FileInfo>();

            if (searchOption == SearchOption.TopDirectoryOnly)
            {
                return GetTopDirectoryFileInfos(dirInfo, fileSearchPattern);
            }

            var stack = new Stack<Node>();

            var root = new Node(dirInfo);

            stack.Push(root);

            if (dirSearchPattern != "" && dirSearchPattern != "*")
            {
                var node = stack.Pop();
                if (node != null)
                {
                    try
                    {
                        var subDirs = node.DirectoryInfo.EnumerateDirectories(dirSearchPattern);
                        foreach (var subDir in subDirs)
                        {
                            stack.Push(new Node(subDir));
                        }
                    }
                    catch (SecurityException ex)
                    {
                        var fileArgs = new FileArgs(null, ex);

                        FileFound?.Invoke(this, fileArgs);
                        if (fileArgs.Cancel)
                        {
                            return fileInfos;
                        }
                    }
                    catch (UnauthorizedAccessException ex)
                    {
                        var fileArgs = new FileArgs(null, ex);

                        FileFound?.Invoke(this, fileArgs);
                        if (fileArgs.Cancel)
                        {
                            return fileInfos;
                        }
                    }

                }
            }

            while (stack.Count != 0)
            {
                var node = stack.Pop();

                if (node != null)
                {
                    FileInfo? curFile = null;
                    try
                    {
                        foreach (var file in node.DirectoryInfo.GetFiles(fileSearchPattern))
                        {
                            FileFound?.Invoke(this, new FileArgs(file));
                            fileInfos.Add(file);

                            if (fileInfos.Count >= _options.FileMaxAmount)
                                throw new FileMaxCountException();
                        }

                        var subDirs = node.DirectoryInfo.EnumerateDirectories(dirSearchPattern);

                        foreach (var subDir in subDirs)
                        {
                            stack.Push(new Node(subDir));
                        }
                    }
                    catch (SecurityException ex)
                    {
                        Console.WriteLine(ex.Message);

                        var fileArgs = new FileArgs(null, ex);

                        FileFound?.Invoke(this, fileArgs);
                        if (fileArgs.Cancel)
                        {
                            break;
                        }
                        continue;
                    }
                    catch (UnauthorizedAccessException ex)
                    {
                        var fileArgs = new FileArgs(null, ex);

                        FileFound?.Invoke(this, fileArgs);
                        if (fileArgs.Cancel)
                        {
                            break;
                        }
                        continue;
                    }
                    catch (FileMaxCountException ex)
                    {
                        var fileArgs = new FileArgs(null, ex);

                        FileFound?.Invoke(this, fileArgs);
                        if (fileArgs.Cancel)
                        {
                            break;
                        }
                        continue;
                    }
                    catch (Exception ex)
                    {
                        Console.WriteLine(ex.ToString());
                        break;
                    }
                }
            }
            return fileInfos;
        }

        List<FileInfo> GetTopDirectoryFileInfos(DirectoryInfo dirInfo, string fileSearchPattern)
        {
            var fileInfos = new List<FileInfo>();

            try
            {
                foreach (var file in dirInfo.EnumerateFiles(fileSearchPattern))
                {
                    FileFound?.Invoke(this, new FileArgs(file));
                    fileInfos.Add(file);

                    if (fileInfos.Count >= _options.FileMaxAmount)
                        throw new FileMaxCountException();
                }
                return fileInfos;
            }
            catch (SecurityException ex)
            {

                var fileArgs = new FileArgs(null, ex);

                FileFound?.Invoke(this, fileArgs);
                if (fileArgs.Cancel)
                {
                    return fileInfos;
                }
            }
            catch (UnauthorizedAccessException ex)
            {
                var fileArgs = new FileArgs(null, ex);

                FileFound?.Invoke(this, fileArgs);
                if (fileArgs.Cancel)
                {
                    return fileInfos;
                }
            }
            catch (FileMaxCountException ex)
            {
                var fileArgs = new FileArgs(null, ex);

                FileFound?.Invoke(this, fileArgs);
                if (fileArgs.Cancel)
                {
                    return fileInfos;
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.ToString());
                return fileInfos;
            }
            return fileInfos;
        }

    }

    class Node
    {
        public Node(DirectoryInfo directoryInfo)
        {
            DirectoryInfo = directoryInfo;
        }
        public DirectoryInfo DirectoryInfo { get; private set; }
    }

}
