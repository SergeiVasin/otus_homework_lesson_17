﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Otus_Homework_Lesson_17
{
    public static class EnumerableExtensions
    {
        public static T? GetMax<T>(this IEnumerable<T> e, Func<T, float> getParameter) where T : class
        {
            T? max = null;

            if (!e.Any()) return null;

            foreach (var item in e)
            {
                if (max == null) max = item;

                max = getParameter(item) > getParameter(max) ? item : max;
            }

            return max;
        }
    }
}
