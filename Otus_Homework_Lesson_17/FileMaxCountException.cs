﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Otus_Homework_Lesson_17
{
    public class FileMaxCountException : Exception
    {
        public override string Message
        {
            get
            {
                return "Достигнуто максимальное количество файлов.";
            }
        }
    }
}
